package com.sveta.dreamgradlekotlin.service.impl

import com.sveta.dreamgradlekotlin.persistence.repository.IProductRepository
import com.sveta.dreamgradlekotlin.service.IProductService
import org.jooq.example.db.h2.public_.tables.pojos.Product
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Service

@Service
class ProductServiceImpl(val productRepository: IProductRepository) : IProductService, ApplicationContextAware {
    override fun setApplicationContext(applicationContext: ApplicationContext) {}

    override fun findById(id: Int): Product? {
        return productRepository.findById(id)
    }

    override fun findAll(): List<Product>? {
        return productRepository.findAll()
    }

    override fun createProduct(product: Product): Int? {
        return productRepository.createProduct(product)
    }

    override fun deleteProduct(id: Int){
        productRepository.deleteProduct(id)
    }

    override fun updateProduct(product: Product): Product? {
        return productRepository.updateProduct(product)
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(ProductServiceImpl::class.java)
    }
}