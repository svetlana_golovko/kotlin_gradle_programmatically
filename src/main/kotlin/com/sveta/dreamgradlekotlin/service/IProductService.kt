package com.sveta.dreamgradlekotlin.service

import org.jooq.example.db.h2.public_.tables.pojos.Product

interface IProductService {
    fun findById(id: Int): Product?
    fun findAll(): List<Product>?
    fun createProduct(product: Product): Int?
    fun deleteProduct(id: Int)
    fun updateProduct(product: Product): Product?
}