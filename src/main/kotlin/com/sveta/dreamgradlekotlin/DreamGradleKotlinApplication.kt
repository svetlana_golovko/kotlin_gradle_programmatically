package com.sveta.dreamgradlekotlin

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import javax.annotation.PostConstruct

@SpringBootApplication
class DreamGradleKotlinApplication() {
    private val LOG: Logger = LoggerFactory.getLogger("DreamGradleKotlinApplication")

    @PostConstruct
    fun postConstruct() {
    }

}

fun main(args: Array<String>) {
    runApplication<DreamGradleKotlinApplication>(*args)
}